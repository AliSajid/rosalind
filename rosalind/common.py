# -*- coding: utf-8 -*-
"""
This file contains common symbols, dicts and other utility functions that can be used elsewhere.
"""

from collections import namedtuple, Counter
from os import path
import itertools

TestData = namedtuple("TestData", ["input", "output"])
FastaSeq = namedtuple("FastaSeq", "name seq")

class Problem:
    ALPHABET = ("A", "C", "G", "T")
    COMPLEMENTS = {"A": "T", "T": "A", "C": "G", "G": "C"}
    MASS = {'A': 71.03711, 'C': 103.00919, 'D': 115.02694, 'E': 129.04259,
                 'F': 147.06841, 'G': 57.02146, 'H': 137.05891, 'I': 113.08406,
                 'K': 128.09496, 'L': 113.08406, 'M': 131.04049, 'N': 114.04293,
                 'P': 97.05276, 'Q': 128.05858, 'R': 156.10111, 'S': 87.03203,
                 'T': 101.04768, 'V': 99.06841, 'W': 186.07931, 'Y': 163.06333}
    CODONS = {'UUU': 'F', 'CUU': 'L', 'AUU': 'I', 'GUU': 'V', 'UUC': 'F', 'CUC': 'L',
                   'AUC': 'I', 'GUC': 'V', 'UUA': 'L', 'CUA': 'L', 'AUA': 'I', 'GUA': 'V',
                   'UUG': 'L', 'CUG': 'L', 'AUG': 'M', 'GUG': 'V', 'UCU': 'S', 'CCU': 'P',
                   'ACU': 'T', 'GCU': 'A', 'UCC': 'S', 'CCC': 'P', 'ACC': 'T', 'GCC': 'A',
                   'UCA': 'S', 'CCA': 'P', 'ACA': 'T', 'GCA': 'A', 'UCG': 'S', 'CCG': 'P',
                   'ACG': 'T', 'GCG': 'A', 'UAU': 'Y', 'CAU': 'H', 'AAU': 'N', 'GAU': 'D',
                   'UAC': 'Y', 'CAC': 'H', 'AAC': 'N', 'GAC': 'D', 'CAA': 'Q', 'AAA': 'K',
                   'GAA': 'E', 'CAG': 'Q', 'AAG': 'K', 'GAG': 'E', 'UGU': 'C', 'CGU': 'R',
                   'AGU': 'S', 'GGU': 'G', 'UGC': 'C', 'CGC': 'R', 'AGC': 'S', 'GGC': 'G',
                   'CGA': 'R', 'AGA': 'R', 'GGA': 'G', 'UGG': 'W', 'CGG': 'R', 'AGG': 'R',
                   'GGG': 'G', 'UAG': 'Stop', 'UGA': 'Stop', 'UAA': 'Stop'}
    REVERSE_CODON_COUNT = Counter(CODONS.values())

    def slice_list(iterable, n, fillvalue=None):
        "Collect data into fixed-length chunks or blocks"
        # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
        args = [iter(iterable)] * n
        return itertools.zip_longest(*args, fillvalue=fillvalue)

    def generate_codons(sequence):
        codons = Problem.slice_list(sequence, 3, "Z")
        return ["".join(c) for c in codons if "Z" not in c]

    def __init__(self, name, run_function, test_data, clean_function = None):

        self.filename = path.join("..", "data", "rosalind_{}.txt".format(name))
        self.run_solution = run_function
        self.clean_data = clean_function
        self.test_input = test_data.input
        self.test_output = test_data.output
        self.output_filename = path.join("..", "output", "output_rosalind_{}.txt".format(name))

    def read_file(self):
        with open(self.filename) as f:
            return f.read().strip()

    def output_file(self):
        with open(self.output_filename, 'w') as f:
            return f.write(str(self.solve_problem()))

    def solve_problem(self, input=None):
        if not input:
            data = self.read_file()
        else:
            data = input
        if self.clean_data:
            cleaned_data = self.clean_data(data)
        else:
            cleaned_data = data
        return self.run_solution(cleaned_data)

    def test_solution(self):
        return self.solve_problem(self.test_input) == self.test_output