# -*- coding: utf-8 -*-
"""
Created on Tue Sep 29 01:45:10 2015

@author: AliSajid
"""
from collections import namedtuple

import regex as re
import requests

from rosalind.common import Problem, TestData

MotifList = namedtuple("MotifList", ["identifier", "matchlist"])


def clean_all(list_of_proteins):
    def cleanup_data(file):
        url = "http://www.uniprot.org/uniprot/{}.fasta".format(file)
        print("Downloading data for {}".format(file))
        req = requests.get(url)
        if req.status_code == 200:
            return "".join(req.text.splitlines()[1:])

    return [(name, cleanup_data(name)) for name in list_of_proteins.splitlines()]


def all_motif_search(list_of_data):
    def motif_search(ident, protein_sequence):

        motif = re.compile(r'N[^P][ST][^P]')
        matches = list(motif.finditer(protein_sequence, overlapped=True))
        matchindices = [str(item.start() + 1) for item in matches]
        if matches:
            return MotifList(identifier=ident, matchlist=" ".join(matchindices))
        else:
            return MotifList(identifier="", matchlist="")

    def format_output(motif_list):
        pat = "{identifier}\n{matchlist}"
        out_strings = [pat.format(**motif._asdict()) for motif in motif_list]
        return "\n".join(out_strings)

    searches = [motif_search(*prot) for prot in list_of_data]

    return format_output(searches).strip()


test = TestData(input="""A2Z669
B5ZC00
P07204_TRBM_HUMAN
P20840_SAG1_YEAST""", output="""B5ZC00
85 118 142 306 395
P07204_TRBM_HUMAN
47 115 116 382 409
P20840_SAG1_YEAST
79 109 135 248 306 348 364 402 485 501 614""")

if __name__ == "__main__":
    mprt = Problem(name="mprt", run_function=all_motif_search, clean_function=clean_all, test_data=test)

    print(mprt.test_solution())
    print(mprt.solve_problem())
    mprt.output_file()
