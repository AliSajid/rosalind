# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 03:30:31 2015

@author: AliSajid
"""
from itertools import permutations

from rosalind.common import Problem, TestData


def clean_data(num):
    return [str(n) for n in range(1, int(num) + 1)]


def get_permutations(data):
    perms = permutations(data)
    list_of_perms = [" ".join(perm_item) for perm_item in perms]
    count = len(list_of_perms)

    return "{}\n{}".format(count, "\n".join(list_of_perms))


test = TestData(input="3", output="""6
1 2 3
1 3 2
2 1 3
2 3 1
3 1 2
3 2 1""")

if __name__ == "__main__":
    perm = Problem("perm", run_function=get_permutations, clean_function=clean_data, test_data=test)

    print(perm.test_solution())
    print(perm.solve_problem())
    perm.output_file()
