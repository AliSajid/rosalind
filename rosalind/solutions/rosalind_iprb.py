# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 18:59:49 2015

@author: AliSajid

"""

from collections import namedtuple

from rosalind.common import Problem, TestData


class PopulationTriplet(namedtuple("PopulationTriplet", "k m n")):
    """

    """

    def total_1(self):
        return sum(self)

    def total_2(self):
        return sum(self) - 1

    def k_by_k(self):
        return self.k / self.total_1() * (self.k - 1) / self.total_2()

    def k_by_m(self):
        return self.k / self.total_1() * self.m / self.total_2()

    def k_by_n(self):
        return self.k / self.total_1() * self.n / self.total_2()

    def m_by_m(self):
        return self.m / self.total_1() * (self.m - 1) / self.total_2()

    def m_by_n(self):
        return self.m / self.total_1() * self.n / self.total_2()

    def n_by_n(self):
        return self.n / self.total_1() * (self.n - 1) / self.total_2()

def split_arguments(data):
    return [int(x) for x in data.split(" ")]

def find_probability(data):
    population = PopulationTriplet(*data)

    k_first_p = population.k_by_k() + population.k_by_m() + population.k_by_n()
    m_first_p = population.k_by_m() + (population.m_by_m() * 0.75) + (population.m_by_n() * 0.5)
    n_first_p = population.k_by_n() + (population.m_by_n() * 0.5)

    return round(k_first_p + m_first_p + n_first_p, 5)



test = TestData("2 2 2", 0.78333)

if __name__ == "__main__":
    iprb = Problem(name="iprb", run_function=find_probability, clean_function=split_arguments, test_data=test)

    print(iprb.solve_problem(test.input))
    print(iprb.test_solution())
    print(iprb.solve_problem())
