# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 17:40:03 2015

@author: AliSajid
"""

from rosalind.common import Problem, TestData

def reverse_and_complement(data):

    return "".join(reversed([Problem.COMPLEMENTS[base] for base in data]))


test = TestData("AAAACCCGGT", "ACCGGGTTTT")

if __name__ == "__main__":
    revc = Problem(name="revc", run_function=reverse_and_complement, test_data=test)
    print(revc.test_solution())
    print(revc.solve_problem())
    revc.output_file()
