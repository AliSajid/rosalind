# -*- coding: utf-8 -*-
"""


"""

from rosalind.common import Problem, TestData, FastaSeq


def clean_fasta(data):
    lines = data.split('>')[1:]

    def clean_element(elem):
        name, seq = elem.split('\n', maxsplit=1)
        seq = seq.replace("\n", "")
        return FastaSeq(name, seq)

    return [clean_element(e) for e in lines]


def generate_overlap_graphs(data):
    suffix_length = 3

    def check_for_overlap(seq_a, seq_b, suffix_length):
        if seq_a == seq_b:
            return
        if seq_a.seq[-suffix_length:] == seq_b.seq[:suffix_length]:
            return seq_a.name, seq_b.name

    overlaps = [pair for pair in
                [check_for_overlap(a, b, suffix_length) for a in data for b in data]
                if pair]

    return "\n".join([" ".join(z) for z in overlaps])


test = TestData('''>Rosalind_0498
AAATAAA
>Rosalind_2391
AAATTTT
>Rosalind_2323
TTTTCCC
>Rosalind_0442
AAATCCC
>Rosalind_5013
GGGTGGG''', """Rosalind_0498 Rosalind_2391
Rosalind_0498 Rosalind_0442
Rosalind_2391 Rosalind_2323""")

if __name__ == "__main__":
    grph = Problem(name="grph", run_function=generate_overlap_graphs, clean_function=clean_fasta, test_data=test)

    print(grph.test_solution())
    print(grph.solve_problem())
    grph.output_file()
