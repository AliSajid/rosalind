# -*- coding: utf-8 -*-
"""


"""

from rosalind.common import Problem, TestData, FastaSeq


def clean_fasta(data):
    lines = data.split('>')[1:]

    def clean_element(elem):
        name, seq = elem.split('\n', maxsplit=1)
        seq = seq.replace("\n", "")
        return FastaSeq(name, seq)

    return [clean_element(e) for e in lines]


def find_lcsm(data):
    data = [x.seq for x in data]

    shortest = min(data, key=len)
    for length in range(len(shortest), 0, -1):
        for start in range(len(shortest) - length + 1):
            sub = shortest[start:start + length]
            if all(seq.find(sub) >= 0 for seq in data):
                return sub
    return ""


test = TestData('''>Rosalind_1
GATTACA
>Rosalind_2
TAGACCA
>Rosalind_3
ATACA''', "TA")

if __name__ == "__main__":
    lcsm = Problem(name="lcsm", run_function=find_lcsm, clean_function=clean_fasta, test_data=test)

    print(lcsm.test_solution())
    print(lcsm.solve_problem())
    lcsm.output_file()
