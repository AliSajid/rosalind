# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 17:40:03 2015

@author: AliSajid
"""

from rosalind.common import Problem, TestData, FastaSeq
from rosalind.solutions.rosalind_revc import reverse_and_complement
import re

def clean_fasta(data):
    lines = data.split('>')[1:]

    def clean_element(elem):
        name, seq = elem.split('\n', maxsplit=1)
        seq = seq.replace("\n", "")
        return FastaSeq(name, seq)

    elems = [clean_element(e) for e in lines]

    if len(elems) == 1:
        return elems[0]
    else:
        return elems


def reverse_palindrome_search(data):

    bounds = range(4,13)
    slices = {}
    for bound in bounds:
        pattern = re.compile('(?=(.{' + str(bound) + '}))')
        slices[bound] = list(pattern.finditer(data.seq))

    successes = {}

    for key in slices:
        for match in slices[key]:
            target = match.groups()[0]
            if reverse_and_complement(target) == target:
                if key in successes:
                    successes[key].append(match)
                else:
                    successes[key] = [match]

    output = []

    for key in successes:
        for success in successes[key]:
            output.append((str(success.regs[0][0] + 1), str(key)))

    return "\n".join([" ".join(res) for res in output])


test = TestData(input=""">Rosalind_24
TCAATGCATGCGGGTCTATATGCAT""", output="""
4 6
5 4
6 6
7 4
17 4
18 4
20 6
21 4""")

if __name__ == "__main__":
    revp = Problem(name="revp", run_function=reverse_palindrome_search, test_data=test, clean_function=clean_fasta)
    print(revp.test_solution())
    print(revp.solve_problem())
    revp.output_file()
