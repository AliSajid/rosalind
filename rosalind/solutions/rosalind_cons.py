# -*- coding: utf-8 -*-
"""


"""

import numpy as np

from rosalind.common import Problem, TestData, FastaSeq


def clean_fasta(data):
    lines = data.split('>')[1:]

    def clean_element(elem):
        name, seq = elem.split('\n', maxsplit=1)
        seq = seq.replace("\n", "")
        return FastaSeq(name, seq)

    return [clean_element(e) for e in lines]

def find_consensus(data):
    data = [x.seq for x in data]

    string_length = len(data[0])

    def transpose(items, length = 4, join=False):
        result = [[str(sub[n]) for sub in items] for n in range(length)]
        if join:
            return ["".join([sub[n] for sub in items]) for n in range(length)]
        else:
            return result

    def count_base(transposed_data):
        return [list(i.count(BASE) for BASE in Problem.ALPHABET) for i in transposed_data]

    def generate_consensus(counted_data):
        return "".join([Problem.ALPHABET[np.argmax(l)] for l in counted_data])

    def generate_output():
        transposed_data = transpose(data, string_length, join=True)
        count = count_base(transposed_data)
        count_t = transpose(count)
        consensus = generate_consensus(count)
        template = """{cons}
A: {a_counts}
C: {c_counts}
G: {g_counts}
T: {t_counts}"""
        return template.format(cons=consensus, a_counts = " ".join(count_t[0]), c_counts = " ".join(count_t[1]),
                               g_counts=" ".join(count_t[2]), t_counts = " ".join(count_t[3]))

    return generate_output()




test = TestData('''>Rosalind_1
ATCCAGCT
>Rosalind_2
GGGCAACT
>Rosalind_3
ATGGATCT
>Rosalind_4
AAGCAACC
>Rosalind_5
TTGGAACT
>Rosalind_6
ATGCCATT
>Rosalind_7
ATGGCACT''', """ATGCAACT
A: 5 1 0 0 5 5 0 0
C: 0 0 1 4 2 0 6 1
G: 1 1 6 3 0 1 0 0
T: 1 5 0 0 0 1 1 6""")

if __name__ == "__main__":
    cons = Problem(name="cons", run_function=find_consensus, clean_function=clean_fasta, test_data=test)

    print(cons.solve_problem(test.input))
    print(cons.test_solution())
    print(cons.solve_problem())
    cons.output_file()
