# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 21:43:07 2015

@author: AliSajid
"""
from rosalind.common import Problem, TestData


def generate_proteins(codon_list):

    def generate_codons(strand):
        start = 0
        while start < len(strand):
            yield (strand[start:start + 3])
            start += 3

    def lookup_amino_acid(codon):
        val = Problem.CODONS[codon]
        if val == 'Stop':
            pass
        else:
            return val

    aa_list = [lookup_amino_acid(codon) for codon in generate_codons(codon_list)]

    if None in aa_list:
        aa_list.remove(None)

    return ''.join(aa_list)


test = TestData("AUGGCCAUGGCGCCCAGAACUGAGAUCAAUAGUACCCGUAUUAACGGGUGA", "MAMAPRTEINSTRING")

if __name__ == "__main__":
    prot = Problem(name="prot", run_function=generate_proteins, test_data=test)

    print(prot.test_solution())
    print(prot.solve_problem())
    prot.output_file()
