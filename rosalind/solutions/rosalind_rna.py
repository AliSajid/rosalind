"""
Stuff
"""

from rosalind.common import Problem, TestData

def convert_rna(data):
    return data.replace("T", "U")


test = TestData("GATGGAACTTGACTACGTAAATT", "GAUGGAACUUGACUACGUAAAUU")

if __name__ == "__main__":
    rna = Problem(name="rna", run_function=convert_rna, test_data=test)

    print(rna.test_solution())
    print(rna.solve_problem())
    rna.output_file()
