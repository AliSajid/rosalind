# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 03:30:31 2015

@author: AliSajid
"""
from itertools import product

from rosalind.common import Problem, TestData


def clean_data(data):
    alphabet, num = data.splitlines()
    alphabet = alphabet.split()
    return {"alphabet": alphabet, "num": num}


def get_cartesian_product(data):
    products = product(data.get("alphabet"), repeat=int(data.get("num")))
    joined = ["".join(l) for l in products]
    return "\n".join(joined)


test = TestData(input="""A C G T
2""", output="""AA
AC
AG
AT
CA
CC
CG
CT
GA
GC
GG
GT
TA
TC
TG
TT""")

if __name__ == "__main__":
    lexf = Problem("lexf", run_function=get_cartesian_product, clean_function=clean_data, test_data=test)

    print(lexf.test_solution())
    print(lexf.solve_problem())
    lexf.output_file()
