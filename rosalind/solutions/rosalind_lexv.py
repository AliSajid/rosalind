# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 03:30:31 2015

@author: AliSajid
"""
from functools import partial
from itertools import product, chain

from rosalind.common import Problem, TestData


def clean_data(data):
    alphabet, num = data.splitlines()
    alphabet = alphabet.split()
    return {"alphabet": alphabet, "num": num}


def get_ordered_list(data):
    alphabet = data.get("alphabet")
    num = int(data.get("num"))

    def ordered_string(string, order):
        res = "".join(["{:x}".format(order.index(c)) for c in string])
        return res

    lex_order = partial(ordered_string, order=alphabet)

    output = chain(*[list(product(alphabet, repeat=n)) for n in range(1, num + 1)])
    output = ["".join(item) for item in output]

    return "\n".join(sorted(output, key=lex_order))


test = TestData(input="""D N A
3""", output="""D
DD
DDD
DDN
DDA
DN
DND
DNN
DNA
DA
DAD
DAN
DAA
N
ND
NDD
NDN
NDA
NN
NND
NNN
NNA
NA
NAD
NAN
NAA
A
AD
ADD
ADN
ADA
AN
AND
ANN
ANA
AA
AAD
AAN
AAA""")

if __name__ == "__main__":
    lexv = Problem("lexv", run_function=get_ordered_list, clean_function=clean_data, test_data=test)

    print(lexv.test_solution())
    print(lexv.solve_problem())
    lexv.output_file()
