# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 03:30:31 2015

@author: AliSajid
"""
from rosalind.common import  Problem, TestData

def count_bases(data):
    return " ".join([str(data.count(base)) for base in Problem.ALPHABET])


test = TestData("AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC", "20 12 17 21")

if __name__ == "__main__":
    dna = Problem("dna", run_function=count_bases, test_data=test)

    print(dna.test_solution())
    print(dna.solve_problem())
    dna.output_file()
