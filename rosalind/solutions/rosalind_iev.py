# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 18:59:49 2015

@author: AliSajid

"""

from rosalind.common import Problem, TestData


def split_arguments(data):
    return [int(x) for x in data.split(" ")]


def find_expected_value(data):
    probabilites = [1, 1, 1, 0.75, 0.5, 0]
    pairs = zip(probabilites, data)
    return sum([p[0] * p[1] for p in pairs]) * 2


test = TestData("1 0 0 1 0 1", 3.5)

if __name__ == "__main__":
    iev = Problem(name="iev", run_function=find_expected_value, clean_function=split_arguments, test_data=test)

    print(iev.solve_problem(test.input))
    print(iev.test_solution())
    print(iev.solve_problem())
