# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 19:11:08 2015

@author: AliSajid
"""
from rosalind.common import Problem, TestData

def cleanup_input(data):
    lines = data.split('>')[1:]

    def clean_element(elem):
        name, codes = elem.split('\n', maxsplit=1)
        codes = codes.replace("\n", "")
        return name, codes

    return  dict([clean_element(e) for e in lines])

def calculate_gc_content(data):

    def calculate_content_once(element):
        g_content = element.count("G")
        c_content = element.count("C")
        gc_content = (g_content + c_content) / float(len(strand)) * 100
        return round(gc_content, 8)

    highest = {'name': '', 'strand': '', 'gc_content': 0}

    for name, strand in data.items():
        c = calculate_content_once(strand)
        if (c - highest["gc_content"])  > 0.001:
            highest["name"] = name
            highest["strand"] = strand
            highest["gc_content"] = c

    return "{name}\n{gc_content:.6f}".format(**highest)

test = TestData(""">Rosalind_6404
CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTTCCGGCCTTCCC
TCCCACTAATAATTCTGAGG
>Rosalind_5959
CCATCGGTAGCGCATCCTTAGTCCAATTAAGTCCCTATCCAGGCGCTCCGCCGAAGGTCT
ATATCCATTTGTCAGCAGACACGC
>Rosalind_0808
CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGACCAGCCCGGAC
TGGGAACCTGCGGGCAGTAGGTGGAAT""", """Rosalind_0808
60.919540""")

if __name__ == "__main__":
    gc = Problem(name="gc", run_function=calculate_gc_content, clean_function=cleanup_input, test_data=test)

    print(gc.test_solution())
    print(gc.solve_problem())
    gc.output_file()
