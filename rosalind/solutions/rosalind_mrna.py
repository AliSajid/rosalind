# -*- coding: utf-8 -*-
"""
Created on Tue Sep 29 01:45:10 2015

@author: AliSajid
"""
import operator
from functools import reduce

from rosalind.common import Problem, TestData


def clean_data(protein_sequence):
    return list(protein_sequence) + ["Stop"]


def all_rna_search(list_of_amino_acids):
    counts = [Problem.REVERSE_CODON_COUNT[amino_acid] for amino_acid in list_of_amino_acids]
    num = reduce(operator.mul, counts)
    return num % 1000000


test = TestData(input="MA", output=12)

if __name__ == "__main__":
    mrna = Problem(name="mrna", run_function=all_rna_search, clean_function=clean_data, test_data=test)

    print(mrna.test_solution())
    print(mrna.solve_problem())
    mrna.output_file()
