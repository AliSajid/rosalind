# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 19:00:18 2015

@author: AliSajid
"""

from rosalind.common import Problem, TestData


def make_data(data):
    return [int(x) for x in data.split(" ")]


def calculate_rabbits(data):
    n, m = data
    sequence = list()
    for i in range(n):
        if i < 2:
            # Normal Fibonacci initialization
            total = 1
            sequence.append(total)
        elif (i < m) or (m == 0):
            # Normal Fibonacci calculation
            total = sequence[i - 1] + sequence[i - 2]
            sequence.append(total)
        elif i == m:
            # Now we need R(n - (m + 1)), but i - (m + 1) < 0, so we have to
            # provide the missing value
            total = sequence[i - 1] + sequence[i - 2] - 1
            sequence.append(total)
        else:
            # i - (m + 1) >= 0, so we can get the value from the sequence
            total = sequence[i - 1] + sequence[i - 2] - sequence[i - (m + 1)]
            sequence.append(total)
    return total


test = TestData("6 3", 4)

if __name__ == "__main__":
    fibd = Problem(name="fibd", run_function=calculate_rabbits, clean_function=make_data, test_data=test)

    print(fibd.solve_problem(test.input))
    print(fibd.test_solution())
    print(fibd.solve_problem())
    fibd.output_file()
