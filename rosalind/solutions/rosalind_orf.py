# -*- coding: utf-8 -*-
"""


"""

from rosalind.common import Problem, TestData, FastaSeq
from rosalind.solutions.rosalind_revc import reverse_and_complement
from rosalind.solutions.rosalind_rna import convert_rna

def clean_fasta(data):
    lines = data.split('>')[1:]

    def clean_element(elem):
        name, seq = elem.split('\n', maxsplit=1)
        seq = seq.replace("\n", "")
        return FastaSeq(name, seq)


    return [clean_element(e) for e in lines]

def find_orf(data):
    orig = [d.seq for d in data][0]
    revc = reverse_and_complement(orig)

    def return_frames(sequence):
        return [sequence[i:] for i in range(3)]

    def lookup_amino_acid(codon):
        val = Problem.CODONS[codon]
        if val == 'Stop':
            pass
        else:
            return val

    def make_protein(codons):
        aa_list = [lookup_amino_acid(codon) for codon in codons]
        try:
            start_index = aa_list.index("M")
            end_index = aa_list.index(None, start_index)

            valid_aa = aa_list[start_index:end_index]
            return "".join(valid_aa)

        except ValueError:
            pass

    frames = [convert_rna(seq) for seq in return_frames(orig) + return_frames(revc)]

    codons_list = [Problem.generate_codons(f) for f in frames]

    proteins = [make_protein(cl) for cl in codons_list]

    return "\n".join([p for p in proteins if p is not None])

test = TestData('''>Rosalind_99
AGCCATGTAGCTAACTCAGGTTACATGGGGATGACCCCGCGACTTGGATTAGAGTCTCTTTTGGAATAAGCCTGAATGATCCGAGTAGCATCTCAG''', """MLLGSFRLIPKETLIQVAGSSPCNLS
M
MGMTPRLGLESLLE
MTPRLGLESLLE""")

if __name__ == '__main__':
    orf = Problem(name="orf", run_function=find_orf, clean_function=clean_fasta, test_data=test)

    # print(orf.solve_problem(test.input))
    # print(orf.test_solution())
    # print(orf.solve_problem())
    # orf.output_file()
