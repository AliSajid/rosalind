"""
Hamm
"""

from rosalind.common import Problem, TestData

def cleanup_form(data):
    return data.split('\n')

def calculate_hamm(str_list):
    string1, string2 = str_list
    count = 0
    for num in range(len(string1)):
        if string1[num] != string2[num]:
            count += 1
    return count


test = TestData("GAGCCTACTAACGGGAT\nCATCGTAATGACGGCCT", 7)

if __name__ == "__main__":
    hamm = Problem(name="hamm", run_function=calculate_hamm, clean_function=cleanup_form, test_data=test)

    print(hamm.test_solution())
    print(hamm.solve_problem())
    hamm.output_file()
