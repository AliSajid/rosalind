# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 19:00:18 2015

@author: AliSajid
"""

from rosalind.common import Problem, TestData

def make_data(data):
    return [int(x) for x in data.split(" ")]

def calculate_rabbits(data):
    n, k = data
    if n <= 2:
        return 1
    else:
        return calculate_rabbits((n-2,k)) * k + calculate_rabbits((n-1,k))


test = TestData("5 3", 19)

if __name__ == "__main__":
    fib = Problem(name="fib", run_function=calculate_rabbits, clean_function=make_data, test_data=test)

    print(fib.solve_problem(test.input))
    print(fib.test_solution())
    print(fib.solve_problem())
    fib.output_file()
