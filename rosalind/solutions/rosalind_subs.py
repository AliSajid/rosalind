# -*- coding: utf-8 -*-
"""
Created on Tue Sep 29 01:45:10 2015

@author: AliSajid
"""
import re

from rosalind.common import Problem, TestData


def cleanup_data(data):
    return data.split('\n')

def substring_search(strings):
    string, substring = strings
    pattern = re.compile('(?=(' + substring + '))')
    return " ".join([str(m.start() +1) for m in re.finditer(pattern, string)])


test = TestData("GATATATGCATATACTT\nATAT", "2 4 10")

if __name__ == "__main__":
    subs = Problem(name="subs", run_function=substring_search, clean_function=cleanup_data, test_data=test)
    print(subs.test_solution())
    print(subs.solve_problem())
    subs.output_file()
