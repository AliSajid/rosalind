# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 23:04:43 2015

@author: AliSajid
"""

from rosalind.common import Problem, TestData

def calculate_mass(protein):

    return round(sum([Problem.MASS[acid] for acid in protein]), 3)


test = TestData("SKADYEK", 821.392)

if __name__ == "__main__":
    prtm = Problem(name="prtm", run_function=calculate_mass, test_data=test)

    print(prtm.test_solution())
    print(prtm.solve_problem())
    prtm.output_file()
