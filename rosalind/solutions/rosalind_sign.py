# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 03:30:31 2015

@author: AliSajid
"""
from itertools import permutations

from rosalind.common import Problem, TestData


def clean_data(num):
    positive_numbers = [str(n) for n in range(1, int(num) + 1)]
    negative_numbers = ["-" + n for n in positive_numbers]
    return (int(num), negative_numbers + positive_numbers)


def get_permutations(data):
    def check_inequality(args):
        if len(set([abs(int(n)) for n in args])) == len(args):
            return True
        else:
            return False

    perms = [p for p in permutations(data[1], data[0]) if check_inequality(p)]
    list_of_perms = [" ".join(perm_item) for perm_item in perms]
    count = len(list_of_perms)

    return "{}\n{}".format(count, "\n".join(list_of_perms))


test = TestData(input="2", output="""8
-1 -2
-1 2
1 -2
1 2
-2 -1
-2 1
2 -1
2 1""")

if __name__ == "__main__":
    sign = Problem("sign", run_function=get_permutations, clean_function=clean_data, test_data=test)

    print(sign.test_solution())
    print(sign.solve_problem())
    sign.output_file()
